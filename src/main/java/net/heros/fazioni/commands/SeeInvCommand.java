package net.heros.fazioni.commands;

import net.heros.core.commands.CommandHandler;
import net.heros.core.constants.Permissions;
import net.heros.core.inventory.Item;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public class SeeInvCommand extends CommandHandler
{
    public SeeInvCommand(final JavaPlugin plugin) {
        super("seeinv player", Permissions.USER, plugin);
    }
    
    public boolean run(final CommandSender sender, final String[] args) {
        if (args.length <= 0) {
            return false;
        }
        if (Bukkit.getPlayerExact(args[0]) == null) {
            return false;
        }
        final Player target = Bukkit.getPlayerExact(args[0]);
        this.seeInv((Player)sender, target);
        return false;
    }
    
    private void seeInv(final Player player, final Player target) {
        final Inventory inventory = Bukkit.createInventory((InventoryHolder)null, 45, "! Inventario di " + target.getName());
        final ItemStack[] inv = target.getInventory().getContents();
        final ItemStack[] armor = target.getInventory().getArmorContents();
        for (final ItemStack itemStack : inv) {
            if (itemStack != null) {
                inventory.addItem(itemStack);
            }
        }
        for (int i = 27; i < 38; ++i) {
            inventory.setItem(i, new Item(Material.STAINED_GLASS_PANE).build("&8&m--", 1, 15));
        }
        int x = 36;
        for (final ItemStack itemStack2 : armor) {
            if (itemStack2 == null) {
                ++x;
            }
            else {
                inventory.setItem(x, itemStack2);
                ++x;
            }
        }
        player.openInventory(inventory);
    }
}
