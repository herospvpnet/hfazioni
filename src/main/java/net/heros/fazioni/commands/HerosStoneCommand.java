package net.heros.fazioni.commands;

import net.heros.core.commands.CommandHandler;
import net.heros.core.constants.Permissions;
import net.heros.core.inventory.Item;
import net.heros.core.objects.HPlayer;
import net.heros.core.objects.message.Message;
import net.heros.core.objects.message.MessageType;
import net.heros.fazioni.listeners.InventoryListeners;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

public class HerosStoneCommand extends CommandHandler {
    private static HashMap<String, Inventory> invSaved = new HashMap<>();

    public HerosStoneCommand(JavaPlugin plugin) {
        super("", Permissions.USER, plugin);
    }

    @Override
    public boolean run(CommandSender sender, String[] args) {
        if (InventoryListeners.getNoHerosStone().contains(sender.getName())) {
            new HPlayer(sender).sendMessage(new Message("Attendi 5 secondi prima di riutilizzare l'herosstone", MessageType.ERROR));
            return false;
        }

        Inventory inventory = Bukkit.createInventory(null, 9*3, "HerosStone");
        invSaved.put(sender.getName(), inventory);
        ItemStack item = new Item(Material.STAINED_GLASS_PANE).build("&8&m--", 1, 15);

        for (int i=0; i<9; i++) {
            inventory.setItem(i, item);
        }

        int x=0;
        int y=0;
        for (int i=9; i<18; i++) {
            if (x!=3) {
                x++;
                inventory.setItem(i, item);
                continue;
            }
            if (y!=2) {
                y++;
                continue;
            }
            x=0;
        }

        for (int i=18; i<27; i++) {
            inventory.setItem(i, item);
        }

        inventory.setItem(11, new Item(Material.STAINED_GLASS_PANE).build("&aInformazioni >", 1, 5, "&7Metti qui l'item da potenziare", "&7o l'herosstone"));
        inventory.setItem(15, new Item(Material.STAINED_GLASS_PANE).build("&a< Informazioni", 1, 5, "&7Metti qui l'item da potenziare", "&7o l'herosstone"));

        inventory.setItem(13, new Item(Material.STAINED_GLASS_PANE).build("&eClicca per eseguire l'upgrade", 1, 4,
                "&c&lRICORDA&7 che l'upgrade puo' avere tre risultati: ", " &7- Potenziamento effettuato con successo (&e30%&7)", "&7 - Item distrutto (&c20%&7)", "&7 - Nessun cambiamento (&e50%&7)"));
        ((Player)sender).openInventory(inventory);
        return false;
    }

    public static HashMap<String, Inventory> getInvSaved() {
        return invSaved;
    }
}
