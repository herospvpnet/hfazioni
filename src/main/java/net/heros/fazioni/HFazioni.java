package net.heros.fazioni;

import me.vagdedes.mysql.database.MySQL;
import net.heros.core.commands.CommandHandler;
import net.heros.fazioni.commands.HerosStoneCommand;
import net.heros.fazioni.commands.SeeInvCommand;
import net.heros.fazioni.listeners.*;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class HFazioni extends JavaPlugin
{
    private static HFazioni instance;

    public void onEnable() {
        instance = this;

        this.registerListener(new ConnectionListeners(), new DeathListeners(), new InventoryListeners(), new LagListeners(), new BlockListeners(), new PlayerListeners());
        this.registerCommand("seeinv", new SeeInvCommand(this));
        registerCommand("herosstone", new HerosStoneCommand(this));
        if (!MySQL.isConnected()) {
            MySQL.connect();
        }
    }
    
    protected void registerCommand(final String commandName, final CommandHandler commandHandler) {
        this.getCommand(commandName).setExecutor(commandHandler);
        this.getCommand(commandName).setTabCompleter((TabCompleter)commandHandler);
    }
    
    protected void registerListener(final Listener... listener) {
        for (final Listener listeners : listener) {
            this.getServer().getPluginManager().registerEvents(listeners, (Plugin)this);
        }
    }

    public static HFazioni getInstance() {
        return instance;
    }

}
