package net.heros.fazioni.listeners;

import net.heros.core.inventory.Item;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import java.util.Random;

public class BlockListeners implements Listener {

    @EventHandler
    public void on(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (player.getInventory().getItemInHand() != null && player.getInventory().getItemInHand().hasItemMeta() &&
                player.getInventory().getItemInHand().getItemMeta().getLore() != null
                && ChatColor.stripColor(player.getInventory().getItemInHand().getItemMeta().getLore().get(0)).contains("Livello:")
                && player.getInventory().getItemInHand().getType().name().contains("PICKAXE")) {
            String type = ChatColor.stripColor(player.getInventory().getItemInHand().getItemMeta().getLore().get(1));
            Location b = event.getBlock().getLocation();
            //WORLDGUARD
            switch (type) {
                case "2x1": {
                    b.getBlock().getRelative(0, -1, 0).breakNaturally();
                    break;
                }
                case "3x1": {
                    b.getBlock().getRelative(0, -1, 0).breakNaturally();
                    b.getBlock().getRelative(0, 1, 0).breakNaturally();
                    break;
                }
                case "3x3x3": {
                    Block block;
                    for(int xOff = -1; xOff <= 1; ++xOff){
                        for(int yOff = -1; yOff <= 1; ++yOff){
                            for(int zOff = -1; zOff <= 1; ++zOff){
                                block = b.getBlock().getRelative(xOff, yOff, zOff);
                                block.breakNaturally();
                            }
                        }
                    }
                    break;
                }
            }
            //TODO: PICCONE
        }
    }

    @EventHandler
    public void onRandom(BlockBreakEvent event) {
        Player player = event.getPlayer();
        int n = new Random().nextInt(100) + 1;
        if (n!=15) {
            return;
        }

        player.getInventory().addItem(new Item(Material.GHAST_TEAR).build("&6HerosStone", 1, 0,
                "&7Inserisci questo oggetto in &a/hs&7 con", "&7un'arma/armatura per aumentare il suo &alivello&7."));
    }
}
