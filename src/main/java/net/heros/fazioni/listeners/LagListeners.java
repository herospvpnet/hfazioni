package net.heros.fazioni.listeners;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class LagListeners implements Listener
{
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntitySpawn(final EntitySpawnEvent event) {
        final Entity entity = event.getEntity();
        final Location entityLocation = event.getLocation();
        int onChunk = 0;
        for (Entity chunkEntity : entityLocation.getWorld().getEntities()) {
            if (entityLocation.getChunk().equals(chunkEntity.getLocation().getChunk())) {
                ++onChunk;
            }
        }
        if (onChunk >= 20) {
            event.setCancelled(true);
        }
    }
}
