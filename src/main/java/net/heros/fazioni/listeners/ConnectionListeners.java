package net.heros.fazioni.listeners;

import org.bukkit.event.player.*;
import net.heros.core.objects.*;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.*;

public class ConnectionListeners implements Listener
{
    @EventHandler
    public void onJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        final HPlayer hPlayer = new HPlayer(player);
        hPlayer.setTab(ChatColor.GOLD + "" + ChatColor.BOLD + "   HEROSPVP " + ChatColor.YELLOW + "| Fazioni" + ChatColor.GRAY + " [Season 1]", ChatColor.WHITE + "mc.herospvp.net");
    }
}
