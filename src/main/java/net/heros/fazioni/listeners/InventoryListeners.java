package net.heros.fazioni.listeners;

import net.heros.core.inventory.Item;
import net.heros.core.objects.HPlayer;
import net.heros.core.objects.message.Message;
import net.heros.core.objects.message.MessageType;
import net.heros.fazioni.HFazioni;
import net.heros.fazioni.commands.HerosStoneCommand;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class InventoryListeners implements Listener {
    private static ArrayList<String> noHerosStone = new ArrayList<>();
    private HashMap<String, Boolean> type = new HashMap<>();

    @EventHandler
    public void on(InventoryClickEvent event) {
        if (event.getInventory().getTitle() != null && event.getInventory().getTitle().contains("! Inventario di")) {
            event.setResult(Event.Result.DENY);
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void on(InventoryCloseEvent event) {
        Inventory inventory = event.getInventory();
        if (!type.containsKey(event.getPlayer().getName()) && inventory.getTitle() != null && inventory.getTitle().equalsIgnoreCase("HerosStone") &&
                inventory.getItem(13) != null && isUpgradeable(inventory.getItem(13))) {
            event.getPlayer().getInventory().addItem(inventory.getItem(13));
            return;
        }

        if (type.containsKey(event.getPlayer().getName()) && type.get(event.getPlayer().getName())) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(HFazioni.getInstance(), new Runnable() {
                @Override
                public void run() {
                    event.getPlayer().openInventory(HerosStoneCommand.getInvSaved().get(event.getPlayer().getName()));
                }
            }, 2L);
        }

        if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equalsIgnoreCase("HerosStone")) {
            if (inventory.getItem(12) != null && inventory.getItem(12).getType() != Material.STAINED_GLASS_PANE) {
                event.getPlayer().getInventory().addItem(inventory.getItem(12));
            }
            if (inventory.getItem(14) != null && inventory.getItem(12).getType() != Material.STAINED_GLASS_PANE) {
                event.getPlayer().getInventory().addItem(inventory.getItem(14));
            }
        }
    }

    @EventHandler
    public void onAnvil(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        HPlayer hPlayer = new HPlayer(player);
        if (!(event.getInventory().getTitle() != null && event.getInventory().getTitle().equalsIgnoreCase("HerosStone"))) {
            return;
        }
        Inventory inventory = event.getInventory();

        if (event.getInventory() != null && event.getInventory().getTitle().equalsIgnoreCase("HerosStone")) {
            if (type.containsKey(player.getName()) && type.get(player.getName())) {
                event.setResult(Event.Result.DENY);
                event.setCancelled(true);
                return;
            }

            if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.STAINED_GLASS_PANE) {
                ItemStack item = event.getCurrentItem();
                event.setCancelled(true);
                event.setResult(Event.Result.DENY);

                if (item.getData().getData() == 4) {
                    if (!(isHerosStone(inventory.getItem(12)) || isHerosStone(inventory.getItem(14)))) {
                        hPlayer.sendMessage(new Message("Non trovo l'herosstone...", MessageType.ERROR));
                        player.closeInventory();
                        return;
                    }

                    amount(inventory, player, 12);
                    amount(inventory, player, 14);

                    if (!(isUpgradeable(inventory.getItem(12)) || isUpgradeable(inventory.getItem(14)))) {
                        hPlayer.sendMessage(new Message("Non trovo l'item da potenziare...", MessageType.ERROR));
                        player.closeInventory();
                        return;
                    }

                    player.playSound(player.getLocation(), Sound.valueOf("SUCCESSFUL_HIT"), 10, 2);
                    type.put(player.getName(), true);

                    ItemStack upgrade;
                    if (isUpgradeable(inventory.getItem(12))) {
                        upgrade = inventory.getItem(12);
                    } else {
                        upgrade = inventory.getItem(14);
                    }

                    if (getLevel(upgrade) != 0) {
                        String name = upgrade.getType().name();

                        if (name.contains("SWORD")) {
                            if (getLevel(upgrade) == 12) {
                                maxItem(player);
                                return;
                            }
                        } else if (name.contains("_AXE")) {
                            if (getLevel(upgrade) == 2) {
                                maxItem(player);
                                return;
                            }
                        } else if (name.contains("CHESTPLATE") || name.contains("LEGGINGS") || name.contains("BOOTS") || name.contains("HELMET")) {
                            if (getLevel(upgrade) == 14) {
                                maxItem(player);
                                return;
                            }
                        } else if (name.contains("PICKAXE")) {
                            if (getLevel(upgrade) == 3) {
                                maxItem(player);
                                return;
                            }
                        }
                    }

                    inventory.clear();
                    inventory.setItem(13, upgrade);

                    ItemStack panel = new Item(Material.STAINED_GLASS_PANE).build("&8&m--", 1, 15);
                    square(inventory, panel, 0);
                    square(inventory, panel, 6);

                    int[] n = {
                      3,4,5,
                      14,23,22,21,12,
                    };
                    for (int i=0; i<8; i++) {
                        inventory.setItem(n[i], new Item(Material.STAINED_GLASS_PANE).build("&8&m--", 1, 7));
                    }

                    final int[] x = {0};
                    new BukkitRunnable() {
                        public void run() {
                            if (x[0]==8) {
                                // int random = new Random().nextInt(10)+1;
                                int random = 1;
                                switch (random) {
                                    case 1: {
                                        done(player,n,inventory);
                                        break;
                                    }
                                    case 2: {
                                        done(player,n,inventory);
                                        break;
                                    }
                                    case 3: {
                                        nothing(inventory,n,player);
                                        break;
                                    }
                                    case 4: {
                                        nothing(inventory,n,player);
                                        break;
                                    }
                                    case 5: {
                                        nothing(inventory,n,player);
                                        break;
                                    }
                                    case 6: {
                                        nothing(inventory,n,player);
                                        break;
                                    }
                                    case 7: {
                                        nothing(inventory,n,player);
                                        break;
                                    }
                                    case 8: {
                                        destroy(inventory,n,player);
                                        break;
                                    }
                                    case 9: {
                                        done(player,n,inventory);
                                        break;
                                    }
                                    case 10: {
                                        destroy(inventory,n,player);
                                        break;
                                    }
                                }

                                cancel();
                                return;
                            }
                            player.playSound(player.getLocation(), Sound.valueOf("FIREWORK_BLAST"), 10, 2);
                            inventory.setItem(n[x[0]], new Item(Material.STAINED_GLASS_PANE).build("&e***", 1, 1));
                            x[0]++;
                        }
                    }.runTaskTimer(HFazioni.getInstance(), 10L, 10L);
                }
            }
        }
    }

    private boolean isHerosStone(ItemStack item) {
        if (item == null) {
            return false;
        }
        return item.getType() == Material.GHAST_TEAR && item.hasItemMeta() && item.getItemMeta().getDisplayName() != null
                && ChatColor.stripColor(item.getItemMeta().getDisplayName()).equalsIgnoreCase("HerosStone") &&
                item.getItemMeta().getLore() != null && ChatColor.stripColor(item.getItemMeta().getLore().get(0)).contains("Inserisci questo");
    }

    private boolean isUpgradeable(ItemStack item) {
        if (item == null) {
            return false;
        }
        String nameItem = item.getType().name();
        return nameItem.contains("CHESTPLATE") || nameItem.contains("LEGGINGS") || nameItem.contains("BOOTS") ||
                nameItem.contains("HELMET") || nameItem.contains("SWORD") || nameItem.contains("_AXE") || nameItem.contains("PICKAXE");
    }

    private void square(Inventory inventory, ItemStack panel, int coeff) {
        int x=coeff;
        int y=0;
        for (int i=coeff; i<coeff+11; i++) {
            if (x!=coeff+3) {
                inventory.setItem(i+y, panel);
                x++;
                continue;
            }
            y+=5;
            x=coeff;
        }
    }

    private void amount(Inventory inventory, Player player, int slot) {
        if (inventory.getItem(slot).getType() == Material.GHAST_TEAR) {
            if (inventory.getItem(slot).getAmount() > 1) {
                int amount = inventory.getItem(slot).getAmount();
                inventory.getItem(slot).setAmount(amount-1);
                if (player.getInventory().firstEmpty() == -1) {
                    player.getWorld().dropItem(player.getLocation(), inventory.getItem(slot));
                    new HPlayer(player).sendMessage(new Message("Il tuo inventario e' pieno, ho droppato l'herosstone a terra", MessageType.WARNING));
                } else {
                    player.getInventory().addItem(inventory.getItem(slot));
                }
                inventory.getItem(slot).setAmount(1);
            }
        }
    }

    private int getLevel(ItemStack itemStack) {
        try {
            return Integer.valueOf(itemStack.getItemMeta().getLore().get(0).split(" ")[1]);
        } catch (Exception e) {
            return 0;
        }
    }

    private void maxItem(Player player) {
        HPlayer hPlayer = new HPlayer(player);
        hPlayer.sendMessage(new Message("Hai raggiunto il livello massimo per questo item", MessageType.INFO));
        type.remove(player.getName());
        HerosStoneCommand.getInvSaved().remove(player.getName());
        player.closeInventory();
    }

    private void close(Inventory inventory, Player player) {
        noHerosStone.add(player.getName());
        Bukkit.getScheduler().scheduleSyncDelayedTask(HFazioni.getInstance(), new Runnable() {
            @Override
            public void run() {
                if (inventory.getTitle() != null && inventory.getTitle().equalsIgnoreCase("HerosStone")) {
                    if (player.getInventory().firstEmpty() == -1 && inventory.getItem(13) != null && inventory.getItem(13).hasItemMeta()
                            && inventory.getItem(13).getItemMeta().getLore() != null
                            && ChatColor.stripColor(inventory.getItem(13).getItemMeta().getLore().get(0)).contains("Livello:")) {
                        player.getWorld().dropItem(player.getLocation(), inventory.getItem(13));
                        new HPlayer(player).sendMessage(new Message("Il tuo inventario e' pieno, ho droppato l'item a terra", MessageType.WARNING));
                    }
                    player.closeInventory();
                }
                noHerosStone.remove(player.getName());
            }
        }, 100L);

        HerosStoneCommand.getInvSaved().remove(player.getName());
        type.remove(player.getName());
    }

    private void done(Player player,int n[], Inventory inventory) {
        player.playSound(player.getLocation(), Sound.valueOf("FIREWORK_BLAST"), 10, 0);
        player.playEffect(player.getLocation(), Effect.SMOKE, 500);
        for (int i=0; i<8; i++) {
            inventory.setItem(n[i], new Item(Material.STAINED_GLASS_PANE)
                    .build("&aPotenziamento riuscito!", 1, 13, "&7Puoi prendere il tuo item"));
        }
        ItemStack upgrade = inventory.getItem(13);
        String name = upgrade.getType().name();

        double coeff = 0;
        String lore = "";

        String[] pick = {
                "", "2x1", "3x1", "3x3x3"
        };
        if (name.contains("SWORD")) {
            coeff = 0.1;
            lore = "danno in piu'";
        } else if (name.contains("_AXE")) {
            coeff = 1;
            lore = "rimozione utilizzi";
        } else if (name.contains("CHESTPLATE") || name.contains("HELMET") || name.contains("LEGGINGS") || name.contains("BOOTS")) {
            coeff = 0.04;
            lore = "protezione in piu'";
        }

        int level = 0;
        if (name.contains("PICKAXE")) {
            if (upgrade.hasItemMeta() && upgrade.getItemMeta().getLore() != null) {
                ItemMeta itemMeta = upgrade.getItemMeta();
                if (ChatColor.stripColor(upgrade.getItemMeta().getLore().get(0)).contains("Livello: ")) {
                    level = Integer.valueOf(upgrade.getItemMeta().getLore().get(0).split(" ")[1])+1;

                    itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "Livello" + ChatColor.YELLOW + ": "
                            +level, ChatColor.RED + "" +pick[level]));
                    upgrade.setItemMeta(itemMeta);
                }
            } else {
                ItemMeta itemMeta = upgrade.getItemMeta();
                itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "Livello" + ChatColor.YELLOW + ": 1", ChatColor.RED + "" + pick[level+1]));
                upgrade.setItemMeta(itemMeta);
            }
        } else {
            if (upgrade.hasItemMeta() && upgrade.getItemMeta().getLore() != null) {
                ItemMeta itemMeta = upgrade.getItemMeta();
                if (ChatColor.stripColor(upgrade.getItemMeta().getLore().get(0)).contains("Livello: ")) {
                    level = Integer.valueOf(upgrade.getItemMeta().getLore().get(0).split(" ")[1]) + 1;

                    itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "Livello" + ChatColor.YELLOW + ": "
                            + level, ChatColor.RED + "" + (Math.round((level * coeff) * 10.0) / 10.0) + " " + lore));
                    upgrade.setItemMeta(itemMeta);
                }
            } else {
                level = 1;
                ItemMeta itemMeta = upgrade.getItemMeta();
                itemMeta.setLore(Arrays.asList(ChatColor.GRAY + "Livello" + ChatColor.YELLOW + ": 1", ChatColor.RED + "" + coeff + " " + lore));
                upgrade.setItemMeta(itemMeta);
            }
        }

        for (Player players : Bukkit.getServer().getOnlinePlayers()) {
            if (name.contains("PICKAXE")) {
                level++;
            }
            new HPlayer(players).sendMessage(new Message(ChatColor.GOLD + ""+ ChatColor.UNDERLINE + "[HerosStone]",MessageType.AIR)
                    .setHover(ChatColor.GREEN+ "INFO\n"+ChatColor.GRAY+" Le herosstone possono essere trovate\n"
                            +ChatColor.GRAY+" spaccando i blocchi, durante gli eventi\n"+ChatColor.GRAY+" o votando il server su minecraft-italia")
                    .add(ChatColor.GREEN + " " + player.getName()).setHover(new HPlayer(players).getFactionInfo())
                    .add(ChatColor.WHITE+ " ha potenziato ")
                    .add(ChatColor.DARK_GREEN +
                            inventory.getItem(13).getType().name().replaceAll("_", " ")
                                    .toLowerCase() + ChatColor.WHITE + " al livello " + ChatColor.AQUA + "" + level)
                    .setHover(ChatColor.GREEN+ "INFO\n"+ChatColor.GRAY+" Le herosstone possono essere trovate\n"
                            +ChatColor.GRAY+" spaccando i blocchi, durante gli eventi\n"+ChatColor.GRAY+" o votando il server su minecraft-italia"));
        }

        close(inventory, player);
    }

    public static ArrayList<String> getNoHerosStone() {
        return noHerosStone;
    }

    private void nothing(Inventory inventory, int[] n, Player player) {
        player.playSound(player.getLocation(), Sound.valueOf("FIREWORK_BLAST"), 10, 0);
        player.playEffect(player.getLocation(), Effect.SMOKE, 500);
        for (int i=0; i<8; i++) {
            inventory.setItem(n[i], new Item(Material.STAINED_GLASS_PANE)
                    .build("&eNon e' cambiato nulla...", 1, 0, "&7Puoi prendere il tuo item"));
        }

        close(inventory, player);
    }

    private void destroy(Inventory inventory, int[] n, Player player) {
        player.playSound(player.getLocation(), Sound.valueOf("FIREWORK_BLAST"), 10, 0);
        player.playEffect(player.getLocation(), Effect.SMOKE, 500);
        inventory.setItem(13, new ItemStack(Material.AIR));
        for (int i=0; i<8; i++) {
            inventory.setItem(n[i], new Item(Material.STAINED_GLASS_PANE)
                    .build("&cHai perso il tuo item!", 1, 14));
        }

        close(inventory, player);
    }
}
