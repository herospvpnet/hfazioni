package net.heros.fazioni.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerListeners implements Listener {

    @EventHandler
    public void on(EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player)) {
            return;
        }
        Player player = (Player) event.getDamager();


        if (isUpgraded(player.getItemInHand())) {
            double coeff = getCoeff(player.getItemInHand());
            double coeffArmor=0;

            if (event.getEntity() instanceof Player) {
                Player victim = (Player) event.getEntity();
                for (ItemStack itemStack : victim.getInventory().getArmorContents()) {
                    if (itemStack == null) {
                        continue;
                    }

                    if (isUpgraded(itemStack)) {
                        coeffArmor+=getCoeff(itemStack);
                    }
                }
            }

            if (player.getItemInHand() != null && player.getItemInHand().getType().name().contains("SWORD")) {
                event.setDamage(event.getDamage(EntityDamageEvent.DamageModifier.BASE) + (coeff) - coeffArmor);
            } else if (player.getItemInHand() != null && player.getItemInHand().getType().name().contains("_AXE")) {
                Player victim = (Player) event.getEntity();
                for (ItemStack itemStack : victim.getInventory().getArmorContents()) {
                    if (itemStack == null) {
                        continue;
                    }
                    itemStack.setDurability((short)((itemStack.getDurability())+((short)getCoeff(player.getItemInHand()))));
                }
            }
        }
    }

    private boolean isUpgraded(ItemStack itemStack) {
        return itemStack.hasItemMeta() && itemStack.getItemMeta().getLore() != null &&
                ChatColor.stripColor(itemStack.getItemMeta().getLore().get(0)).contains("Livello:");
    }

    private double getCoeff(ItemStack itemStack) {
        return Double.valueOf(ChatColor.stripColor(itemStack.getItemMeta().getLore().get(1).split(" ")[0]));
    }
}
