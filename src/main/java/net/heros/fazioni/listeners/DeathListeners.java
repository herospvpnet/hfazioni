package net.heros.fazioni.listeners;

import org.bukkit.event.entity.*;
import net.heros.core.objects.*;
import org.bukkit.*;
import net.heros.core.objects.message.*;
import net.md_5.bungee.api.chat.*;
import org.bukkit.entity.*;
import org.bukkit.event.*;

public class DeathListeners implements Listener
{
    @EventHandler
    public void on(final PlayerDeathEvent event) {
        event.setDeathMessage((String)null);
        if (event.getEntity().getKiller() == null) {
            for (final Player player : Bukkit.getOnlinePlayers()) {
                final HPlayer hPlayer = new HPlayer(player);
                hPlayer.sendMessage(new Message(ChatColor.GRAY + event.getEntity().getName() + " \u00e8 morto.", MessageType.AIR).setHover(new HPlayer(event.getEntity()).getFactionInfo()));
            }
            return;
        }
        final Player killer = event.getEntity().getKiller();
        for (final Player player2 : Bukkit.getOnlinePlayers()) {
            final HPlayer hPlayer2 = new HPlayer(player2);
            hPlayer2.sendMessage(new Message(ChatColor.GRAY + "(!) " + event.getEntity().getName(), MessageType.AIR).setHover(new HPlayer(event.getEntity()).getFactionInfo()).add(ChatColor.GRAY + " \u00e8 stato ucciso da ").add(ChatColor.GRAY + "(!) " + killer.getName()).setClick(ClickEvent.Action.RUN_COMMAND, "/seeinv " + killer.getName()).setHover(new HPlayer(killer).getFactionInfo() + "\n�6�lClicca per vedere il suo inventario").add(ChatColor.GRAY + " con " + (int)killer.getHealth() / 2 + "\u2764"));
        }
    }
}
